
from django.contrib import admin
from django.urls import path, include

#genera el token si este no esta creado
from rest_framework.authtoken import views

from api.views import Login, Logout, items, lista

urlpatterns = [
    path('admin/', admin.site.urls),
    path('api/', include(('api.urls', 'api')) ),
    path('generate_token', views.obtain_auth_token),
]


urlpatterns += [
    path('login/', Login.as_view(), name='login')
]


urlpatterns += [
    path('logout/', Logout.as_view()),
]

urlpatterns += [
    path('setItem/', items)
]


urlpatterns += [
    path('', lista)
]



