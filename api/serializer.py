from rest_framework import serializers
from .models import user, item

class userSerializer(serializers.ModelSerializer):
    class Meta:
        model = user
        fields = '__all__'


class itemSerializer(serializers.ModelSerializer):
    class Meta:
        model = item
        fields = '__all__'