from django.urls import path
from .views import userList

urlpatterns = [
    path('user/', userList.as_view(), name='user')
]